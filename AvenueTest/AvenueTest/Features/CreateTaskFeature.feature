﻿Feature: Create Task
	As a ToDo App user
	I should be able to create a task
	So I can manage my tasks


Scenario: "My tasks" link is always visible
	Given I am on the Avenue code test site
	And I can see the My tasks link on the NavBar
	When I click on the My tasks link
	Then I should be redirected to a page with all the created tasks so far

Scenario Outline: A message directed to the user is displayed
	Given I am on the Avenue code test site
	And I am logged in as <user>
	When I click on the My tasks link
	Then I should be redirected to a page that has the message <message>
Examples: 
| user | message |
| fernandoadt1@gmail.com | Hey Fernando Teixeira, this is your todo list for today: | 


Scenario: User add tasks clicking on Add Button
	Given I am on the My tasks Page
	And I filled the Task Name Field with "Another Task"
	When I click on the Add Button
	Then A new task should be created and added on the list in the same page

Scenario: User add tasks pressing Enter
	Given I am on the My tasks Page
	And I filled the Task Name Field with "Another Task"
	When I press Enter
	Then A new task should be created and added on the list in the same page

Scenario: Task's name should have at least 3 characters
	Given I am on the My tasks Page
	And I filled the Task Name Field with "ab"
	When I click on the Add Button
	Then The task is not added because the task name limit character rule is not met

Scenario: Task's name should have at most 250 characters
	Given I am on the My tasks Page
	And I filled the Task Name Field with "test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test"
	When I click on the Add Button
	Then The task is not added because the task name limit character rule is not met

Scenario: The added Task is appended on the list of created tasks
	Given I am on the My tasks Page
	And I filled the Task Name Field with "Appended task"
	When I click on the Add Button
	Then The task "Appended task" in appended on the created tasks list
