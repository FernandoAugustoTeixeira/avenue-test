﻿Feature: Create SubTask
	As a ToDo App user
	I should be able to create a subtask
	So I can break down my tasks in smaller pieces

Scenario: The user should see a button labeled as 'Manage Subtasks'
	Given I am on the Avenue code test site
	And I can see the My tasks link on the NavBar
	When I click on the My tasks link
	Then I should see a "Manage Subtasks" button for each task on the list

Scenario: The 'Manage Subtasks' button have the number of subtasks created for that tasks
	Given I am on the My tasks Page
	And I can see the number of created subtasks on the Manage subtasks button 
	When I click on the Manage Subtasks Button
	Then I should see the same number os subtasks on the list

Scenario: The "TaskID" and "TaskDescription" are read only fields
	Given I am on the My tasks Page
	When I click on the Manage Subtasks Button
	Then I should see the TaskID and TaskDescription fields
	And These fields are non-editable

Scenario: Validation of "SubTask Description" and "SubTask DueDate" field formats
	Given I am on the My tasks Page
	When I click on the Manage Subtasks Button
	Then The SubTask Description field should allow just values with at most 250 characters. A negative Example: "test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test test"
	And The SubTask DueDate field should allow just values in the MM/dd/yyyy format. A negative Example: "test123"

Scenario: Creation of SubTasks when the Add Button is pressed
	Given I am on the My tasks Page
	When I click on the Manage Subtasks Button
	And I fill the SubTask Description field with the value "task description"
	And I fill the SubTask Due date field with the value "03/18/2018"
	And I click on the Add Subtask Button 
	Then A new subtask should be created

Scenario: Validation that Task Description is a required field
	Given I am on the My tasks Page
	When I click on the Manage Subtasks Button
	And I don't fill the SubTask Description field
	And I fill the SubTask Due date field with the value "03/18/2018"
	And I click on the Add Subtask Button 
	Then A new subtask should not be created

Scenario: Validation that Due Date is a required field
	Given I am on the My tasks Page
	When I click on the Manage Subtasks Button
	And I fill the SubTask Description field with the value "task description"
	And I don't fill the SubTask Due date
	And I click on the Add Subtask Button 
	Then A new subtask should not be created

Scenario: Added SubTasks are appended on the bottom part of the modal dialog
	Given I am on the My tasks Page
	When I click on the Manage Subtasks Button
	And I fill the SubTask Description field with the value "task description"
	And I fill the SubTask Due date field with the value "03/18/2018"
	And I click on the Add Subtask Button 
	Then A new subtask should be created and appended on the bottom part of the modal dialog
