﻿using AvenueTest.Pages;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using TechTalk.SpecFlow;

namespace AvenueTest.Steps
{
    [Binding]
    public class CreateSubTaskSteps
    {
        private CreateTaskPage createTaskPage;
        private SubTasksModal createSubTaskModal;

        private CreateSubTaskSteps()
        {
            createTaskPage = new CreateTaskPage();
            createSubTaskModal = new SubTasksModal();
        }

        [Then(@"I should see a ""(.*)"" button for each task on the list")]
        public void ThenIShouldSeeAButtonForEachTaskOnTheList(string buttonName)
        {
            Assert.IsTrue(createTaskPage.verifyManageSubstasksButtonForEachTask(buttonName));
        }

        [Given(@"I can see the number of created subtasks on the Manage subtasks button")]
        public void GivenICanSeeTheNumberOfCreatedSubtasksOnTheManageSubtasksButton()
        {
            createTaskPage.numberOnTheLastManageSubTasksButton = createTaskPage.getTheNumberOnTheLastManageSubtasksButton();
        }

        [When(@"I click on the Manage Subtasks Button")]
        public void WhenIClickOnTheManageSubtasksButton()
        {
            createTaskPage.clickOnTheLastManageSubtasksButton();
        }

        [Then(@"I should see the same number os subtasks on the list")]
        public void ThenIShouldSeeTheSameNumberOsSubtasksOnTheList()
        {
            Assert.AreEqual(createTaskPage.numberOnTheLastManageSubTasksButton,createSubTaskModal.getNumberOfRowsSubTasksTable());
        }

        [Then(@"I should see the TaskID and TaskDescription fields")]
        public void ThenIShouldSeeTheTaskIDAndTaskDescriptionFields()
        {
            Assert.IsTrue(createSubTaskModal.isModalTitleVisible());
            Assert.IsTrue(createSubTaskModal.isTaskDescriptionVisible());
        }

        [Then(@"These fields are non-editable")]
        public void ThenTheseFieldsAreNon_Editable()
        {
            Assert.IsFalse(createSubTaskModal.isModalTitleEditable(), "The Task ID is editable");
            Assert.IsFalse(createSubTaskModal.isTaskDescriptionEditable(), "The Task Description is editable");
        }

        [Then(@"The SubTask Description field should allow just values with at most (.*) characters\. A negative Example: ""(.*)""")]
        public void ThenTheSubTaskDescriptionFieldShouldAllowJustValuesWithAtMostCharacters_ANegativeExample(int numberOfCharacters, string description)
        {
            Assert.IsTrue(createSubTaskModal.validateSubTaskDescriptionFormat(numberOfCharacters, description));
        }

        [Then(@"The SubTask DueDate field should allow just values in the MM/dd/yyyy format\. A negative Example: ""(.*)""")]
        public void ThenTheSubTaskDueDateFieldShouldAllowJustValuesInTheMMDdYyyyFormat_ANegativeExample(string date)
        {
            Assert.IsFalse(createSubTaskModal.validateSubTaskDueDateFormat(date));
        }

        [When(@"I fill the SubTask Description field with the value ""(.*)""")]
        public void WhenIFillTheSubTaskDescriptionFieldWithTheValue(string description)
        {
            createSubTaskModal.fillSubTaskDescription(description);
        }

        [When(@"I fill the SubTask Due date field with the value ""(.*)""")]
        public void WhenIFillTheSubTaskDueDateFieldWithTheValue(string date)
        {
            createSubTaskModal.fillSubTaskDueDate(date);
        }

        [When(@"I click on the Add Subtask Button")]
        public void WhenIClickOnTheAddSubtaskButton()
        {
            createSubTaskModal.oldNumberofRows = createSubTaskModal.getNumberOfRowsSubTasksTable();
            createSubTaskModal.pressAddButton();
        }

        [Then(@"A new subtask should be created")]
        public void ThenANewSubtaskShouldBeCreated()
        {
            // compare (old num of rows + 1) and (actual number of rows)
            Assert.AreEqual(createSubTaskModal.oldNumberofRows + 1, createSubTaskModal.getNumberOfRowsSubTasksTable());
        }

        [When(@"I don't fill the SubTask Description field")]
        public void WhenIDonTFillTheSubTaskDescriptionField()
        {
            createSubTaskModal.clearSubTasksDescriptionField();
        }

        [When(@"I don't fill the SubTask Due date")]
        public void WhenIDonTFillTheSubTaskDueDate()
        {
            createSubTaskModal.clearSubTasksDueDateField();
        }

        [Then(@"A new subtask should not be created")]
        public void ThenANewSubtaskShouldNotBeCreated()
        {
            // compare (old num of rows) and (actual number of rows)
            Assert.AreEqual(createSubTaskModal.oldNumberofRows, createSubTaskModal.getNumberOfRowsSubTasksTable());
        }

        [Then(@"A new subtask should be created and appended on the bottom part of the modal dialog")]
        public void ThenANewSubtaskShouldBeCreatedAndAppendedOnTheBottomPartOfTheModalDialog()
        {
            createSubTaskModal.validateTheNameOfTheLastSubTask(createSubTaskModal.subTaskDescription);
        }



    }
}
