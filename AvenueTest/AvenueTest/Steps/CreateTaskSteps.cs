﻿using AvenueTest.Pages;
using AvenueTest.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using System;
using TechTalk.SpecFlow;

namespace AvenueTest.Steps
{
    [Binding]
    public class CreateTaskSteps
    {
        private CreateTaskPage createTaskPage;
        private LoginPage loginPage;
        private string username = "fernandoadt1@gmail.com";
        private string password = "avenuetest";

        private CreateTaskSteps()
        {
            DriverFactory.Initialize();
            loginPage = new LoginPage();
            createTaskPage = new CreateTaskPage();
        }

        [AfterScenario]
        public void AfterScenario()
        {
            DriverFactory.Instance.Quit();
        }

        [Given(@"I am on the Avenue code test site")]
        public void GivenIAmOnTheAvenueCodeTestSite()
        {
            loginPage.signIn(username, password);
            Assert.IsTrue(createTaskPage.validateURL());
        }

        [Given(@"I can see the My tasks link on the NavBar")]
        public void GivenICanSeeTheMyTasksLinkOnTheNavBar()
        {
            Assert.IsTrue(createTaskPage.isMyTasksLinkVisible());
        }

        [Given(@"I am logged in as (.*)")]
        public void GivenIAmLoggedInAs(string p0)
        {
            //compare if the username that was used to signIn and the feature parameter are equals
            Assert.AreEqual(p0, username);
        }

        [When(@"I click on the My tasks link")]
        public void WhenIClickOnTheMyTasksLink()
        {
            createTaskPage.clickOnMyTasksLink();
        }

        [Then(@"I should be redirected to a page with all the created tasks so far")]
        public void ThenIShouldBeRedirectedToAPageWithAllTheCreatedTasksSoFar()
        {
            Assert.IsTrue(createTaskPage.validateURL());
            Assert.IsTrue(createTaskPage.isTasksTableVisible());
        }

        [Then(@"I should be redirected to a page that has the message (.*)")]
        public void ThenIShouldBeRedirectedToAPageThatHasTheMessage(string message)
        {
            Assert.IsTrue(createTaskPage.validatePresentedMessage(message), "The actual message and the expected message are different!");
        }


        [Given(@"I am on the My tasks Page")]
        public void GivenIAmOnTheMyTasksPage()
        {
            loginPage.signIn(username, password);
            Assert.IsTrue(createTaskPage.validateURL());
        }

        [Given(@"I filled the Task Name Field with ""(.*)""")]
        public void GivenIFilledTheTaskNameFieldWith(string taskName)
        {
            createTaskPage.fillTaskNameField(taskName);
        }


        [When(@"I click on the Add Button")]
        public void WhenIClickOnTheAddButton()
        {
            createTaskPage.oldNumberOfRows = createTaskPage.getNumberOfRowsInTaskTable();
            createTaskPage.clickOnAddButton();
        }
        
        [When(@"I press Enter")]
        public void WhenIPressEnter()
        {
            createTaskPage.oldNumberOfRows = createTaskPage.getNumberOfRowsInTaskTable();
            createTaskPage.pressingEnterWithFocusOnTaskNameField();
        }
        
        [Then(@"A new task should be created and added on the list in the same page")]
        public void ThenANewTaskShouldBeCreatedAndAddedOnTheListInTheSamePage()
        {
            // compare (old num of rows + 1) and (actual number of rows)
            Assert.AreEqual(createTaskPage.oldNumberOfRows + 1,createTaskPage.getNumberOfRowsInTaskTable());
        }

        [Then(@"The task is not added because the task name limit character rule is not met")]
        public void ThenTheTaskIsNotAddedBecauseTheTaskNameLimitCharacterRuleIsNotMet()
        {
            Assert.AreEqual(createTaskPage.oldNumberOfRows, createTaskPage.getNumberOfRowsInTaskTable(), "The limit character rule was not met and the task was added in the same manner");
        }

        [Then(@"The task ""(.*)"" in appended on the created tasks list")]
        public void ThenTheTaskInAppendedOnTheCreatedTasksList(string taskname)
        {
            Assert.IsTrue(createTaskPage.verifyLastTaskName(taskname));
        }


    }
}
