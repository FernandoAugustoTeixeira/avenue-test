﻿using AvenueTest.Utils;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace AvenueTest.Pages
{
    public class CreateTaskPage
    {
        private IWebDriver _driver;
        public int oldNumberOfRows {get;set;}
        public int numberOnTheLastManageSubTasksButton { get; set; }

        public CreateTaskPage()
        {
            _driver = DriverFactory.Instance;
            PageFactory.InitElements(_driver, this);
        }

        #region Web Elements

        [FindsBy(How = How.LinkText, Using = "My Tasks")]
        protected IWebElement MytasksLink { get; set; }

        [FindsBy(How = How.XPath, Using = "/html/body/div[1]/h1")]
        protected IWebElement TitleMessageToUser { get; set; }

        [FindsBy(How = How.Id, Using = "new_task")]
        protected IWebElement TaskNameField { get; set; }

        [FindsBy(How = How.ClassName, Using = "glyphicon-plus")]
        protected IWebElement AddButton { get; set; }

        [FindsBy(How = How.ClassName, Using = "table")]
        protected IWebElement TasksTable { get; set; }

        #endregion

        #region Methods

        public bool validateURL()
        {
            return _driver.Url.Equals("https://qa-test.avenuecode.com/tasks");
        }

        public void clickOnMyTasksLink()
        {
            MytasksLink.Click();
        }

        public bool isMyTasksLinkVisible()
        {
            return MytasksLink.Displayed;
        }

        public bool validatePresentedMessage(string message)
        {
            return TitleMessageToUser.Text.Equals(message);
        }

        public bool isTasksTableVisible()
        {
            return TasksTable.Displayed;
        }

        public void fillTaskNameField(string name)
        {
            TaskNameField.SendKeys(name);
        }

        public void clickOnAddButton(){
            AddButton.Click();
        }

        public void pressingEnterWithFocusOnTaskNameField()
        {
            TaskNameField.SendKeys(Keys.Enter);
        }

        public int getNumberOfRowsInTaskTable()
        {
            return TasksTable.FindElements(By.TagName("tr")).Count;
        }

        public bool verifyLastTaskName(string taskname)
        {
            var tableBody = TasksTable.FindElement(By.TagName("tbody"));
            var firstRow = tableBody.FindElements(By.TagName("tr"))[0];
            var taskNameColumn = firstRow.FindElements(By.TagName("td"))[1];
            return taskNameColumn.FindElement(By.TagName("a")).Text.Equals(taskname);
        }

        public bool verifyManageSubstasksButtonForEachTask(string buttonName)
        {
            bool existsManageSubtasksButtons = true;
            var tableBody = TasksTable.FindElement(By.TagName("tbody"));
            foreach (IWebElement row in tableBody.FindElements(By.TagName("tr")))
            {
                if(row.FindElements(By.TagName("td"))[3].Text.Contains(buttonName) == false)
                {
                    existsManageSubtasksButtons = false;
                    break;
                }
            }
            return existsManageSubtasksButtons;
        }

        public int getTheNumberOnTheLastManageSubtasksButton()
        {
            var tableBody = TasksTable.FindElement(By.TagName("tbody"));
            var lastRow = tableBody.FindElements(By.TagName("tr")).Last();
            var text = lastRow.FindElements(By.TagName("td"))[3].FindElement(By.TagName("button")).Text;
            return Int32.Parse(Regex.Match(text, @"\d+").Value);
        }

        public void clickOnTheLastManageSubtasksButton()
        {
            var tableBody = TasksTable.FindElement(By.TagName("tbody"));
            var lastRow = tableBody.FindElements(By.TagName("tr")).Last();
            lastRow.FindElements(By.TagName("td"))[3].FindElement(By.TagName("button")).Click();
        }
        #endregion

    }
}
