﻿using AvenueTest.Utils;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace AvenueTest.Pages
{
    public class SubTasksModal
    {
        private IWebDriver _driver;
        private WebDriverWait wait;
        public int oldNumberofRows;
        public string subTaskDescription;

        public SubTasksModal()
        {
            _driver = DriverFactory.Instance;
            PageFactory.InitElements(_driver, this);
            wait = new WebDriverWait(_driver, TimeSpan.FromSeconds(5));       
        }

        #region Web Elements

        protected IWebElement SubTasksTable { get; set; }

        [FindsBy(How = How.ClassName, Using = "modal-dialog")]
        protected IWebElement ModalDialog { get; set; }

        [FindsBy(How = How.ClassName, Using = "modal-title")]
        protected IWebElement ModalTitle { get; set; }

        [FindsBy(How = How.Id, Using = "edit_task")]
        protected IWebElement TaskDescription { get; set; }

        [FindsBy(How = How.Id, Using = "new_sub_task")]
        protected IWebElement SubTaskDescription { get; set; }

        [FindsBy(How = How.Id, Using = "dueDate")]
        protected IWebElement SubTaskDueDate { get; set; }

        [FindsBy(How = How.Id, Using = "add-subtask")]
        protected IWebElement AddSubTasksButton { get; set; }

        #endregion

        #region Methods

        public int getNumberOfRowsSubTasksTable()
        {
            SubTasksTable = _driver.FindElements(By.TagName("table"))[1];
            //we remove the head row in the count
            return SubTasksTable.FindElements(By.TagName("tr")).Count - 1;
        }

        public bool isModalTitleVisible()
        {
            return ModalTitle.Displayed;
        }

        public bool isTaskDescriptionVisible()
        {
            return TaskDescription.Displayed;
        }

        public bool isModalTitleEditable()
        {
            wait.Until(ExpectedConditions.ElementIsVisible(By.ClassName("modal-dialog")));
            // if the send keys works, then it is editable
            try
            {
                ModalTitle.SendKeys("edit");
            }
            catch (TargetInvocationException)
            {
                return false;
            }
            return true;
        }

        public bool isTaskDescriptionEditable()
        {
            wait.Until(ExpectedConditions.ElementIsVisible(By.ClassName("modal-dialog")));
            // if the send keys works, then it is editable
            try
            {
                TaskDescription.SendKeys("edit");
            }
            catch (TargetInvocationException)
            {
                return false;
            }
            return true;
        }

        public bool validateSubTaskDescriptionFormat(int characters, string description)
        {
            wait.Until(ExpectedConditions.ElementIsVisible(By.ClassName("modal-dialog")));
            SubTaskDescription.SendKeys(description);
            return SubTaskDescription.Text.Length <= characters;
        }

        public bool validateSubTaskDueDateFormat(string date)
        {
            wait.Until(ExpectedConditions.ElementIsVisible(By.ClassName("modal-dialog")));
            SubTaskDueDate.Clear();
            SubTaskDueDate.SendKeys(date);
            return SubTaskDueDate.GetAttribute("value").Equals(date);
        }

        public void fillSubTaskDescription(string description)
        {
            wait.Until(ExpectedConditions.ElementIsVisible(By.ClassName("modal-dialog")));
            subTaskDescription = description;
            SubTaskDescription.SendKeys(description);
        }

        public void fillSubTaskDueDate(string date)
        {
            SubTaskDueDate.Clear();
            SubTaskDueDate.SendKeys(date);
        }

        public void pressAddButton()
        {
            AddSubTasksButton.Click();
        }

        public void clearSubTasksDescriptionField()
        {
            wait.Until(ExpectedConditions.ElementIsVisible(By.ClassName("modal-dialog")));
            SubTaskDescription.Clear();
        }

        public void clearSubTasksDueDateField()
        {
            wait.Until(ExpectedConditions.ElementIsVisible(By.ClassName("modal-dialog")));
            SubTaskDueDate.Clear();
        }

        public bool validateTheNameOfTheLastSubTask(string description)
        {
            SubTasksTable = _driver.FindElements(By.TagName("table"))[1];
            var body = SubTasksTable.FindElement(By.TagName("tbody"));
            var lastRow = body.FindElements(By.TagName("tr")).Last();
            return lastRow.FindElements(By.TagName("td"))[1].Text.Equals(description);
        }

        #endregion
    }
}
