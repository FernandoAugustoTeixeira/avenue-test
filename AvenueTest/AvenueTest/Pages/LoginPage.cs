﻿using AvenueTest.Utils;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AvenueTest.Pages
{
    public class LoginPage
    {
        private IWebDriver _driver;

        public LoginPage()
        {
            _driver = DriverFactory.Instance;
            PageFactory.InitElements(_driver, this);
        }

        #region Web Elements

        [FindsBy(How = How.Id, Using = "user_email")]
        protected IWebElement EmailField { get; set; }

        [FindsBy(How = How.Id, Using = "user_password")]
        protected IWebElement PasswordField { get; set; }

        [FindsBy(How = How.Name, Using = "commit")]
        protected IWebElement SignInButton { get; set; }

        #endregion

        #region Methods

        public void signIn(string email, string password)
        {
            EmailField.SendKeys(email);
            PasswordField.SendKeys(password);
            SignInButton.Click();
        }

        public bool validateURL()
        {
            return _driver.Url.Equals("https://qa-test.avenuecode.com/");
        }

        #endregion

    }
}
