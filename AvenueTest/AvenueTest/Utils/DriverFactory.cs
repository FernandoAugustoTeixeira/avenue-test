﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AvenueTest.Utils
{
    public static class DriverFactory
    {
        public static IWebDriver Instance { get; set; }

        static DriverFactory()
        {
            DriverFactory.Instance = null;
        }

        public static void Initialize()
        {
            var options = new ChromeOptions();
            options.AddArgument("--start-maximized");
            options.AddArgument("--disable-javascript");
            DriverFactory.Instance = new ChromeDriver(options);
            DriverFactory.Instance.Manage().Timeouts().ImplicitWait = (TimeSpan.FromSeconds(5));
            DriverFactory.Instance.Manage().Timeouts().PageLoad = (TimeSpan.FromSeconds(10));
            DriverFactory.Instance.Navigate().GoToUrl("https://qa-test.avenuecode.com/tasks");
        }
    }
}
